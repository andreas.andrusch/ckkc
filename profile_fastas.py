#!/usr/bin/env python3

# auxilliary script to cluster fastas from other clustering methods

import argparse
import sys
import os

import CKKC

import numpy as np
from sklearn.decomposition import PCA
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

class FakeClustering:
    def __init__(self):
        self.n_clusters = 0
        self.labels_ = []
        self.cluster_seq_counter = []
        self.cluster_sizes = []
        self.norm_k_vectors = []
        self.cluster_centers_ = []
        self.sequence_names = []

def setup_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('-k', type=int, default=9, help='kmer size')
    parser.add_argument('-i', default='.', help='input folder (only fastas in there!)')
    parser.add_argument('-o', default='.', help='output folder')
    parser.add_argument('-j', default=1, type=int, help='jobs')
    return parser


def main():
    # setup
    parser = setup_parser()
    cli_args = parser.parse_args(sys.argv[1:])

    logger = CKKC.setup_logger('info')

    # read and profile fastas
    # save number of sequences per cluster
    # save number of bases per cluster
    # save a norm k vector list for every fasta
    full_out = {}
    fastas = []
    for fasta in os.scandir(cli_args.i):
        fastas.append(fasta.name)
        with open(fasta.path) as infile:
            full_out[os.path.splitext(fasta.name)[0]] = CKKC.read_fasta_input_to_norm_k_vectors_threaded(infile,
                                                                                                         cli_args.k,
                                                                                                         cli_args.j,
                                                                                                         logger)
            # this gives us sequence lengths and norm k vectors

    out_dict = {}
    for inner_dict in full_out.values():
        out_dict.update(inner_dict)

    # construct fake clustering object
    clustering = FakeClustering()
    cluster_names = []
    for cluster_index, cluster_name in enumerate(full_out.keys()):
        clustering.n_clusters += 1
        cluster_names.append(cluster_name)
        for seq_name in full_out[cluster_name].keys():
            seq_len, seq_nkv = full_out[cluster_name][seq_name]
            clustering.labels_.append(cluster_index)
            clustering.sequence_names.append(seq_name)
            clustering.norm_k_vectors.append(seq_nkv)

    # count sequence numbers and bases
    clustering.cluster_seq_counter,\
    clustering.cluster_sizes = CKKC.calculate_cluster_sizes(clustering, clustering.sequence_names, out_dict)

    # calculate cluster centers
    clustering.cluster_centers_ = CKKC.calculate_cluster_centers(clustering.n_clusters,clustering.labels_, clustering.norm_k_vectors)

    highest_cluster_index = max(clustering.labels_)
    # produce statistics
    # write out cluster num, seq num, base num, standard deviation, (weighted sd), central point
    logger.info('Generating statistics for clustering')
    os.makedirs(cli_args.o, exist_ok=True)
    with open(os.path.join(cli_args.o, 'clustering_stats.csv'), 'w') as csv_file:
        csv_file.write('Cluster\tNumber of sequences\tNumber of bases\tStandard deviation\tCentral point\n')
        # get all sequences of every cluster into one list to calculate sd
        cluster_sequences = {}
        for sequence_index, cluster_label in enumerate(clustering.labels_):
            try:
                cluster_sequences[cluster_label].append(clustering.norm_k_vectors[sequence_index])
            except KeyError:
                cluster_sequences[cluster_label] = [clustering.norm_k_vectors[sequence_index]]
        # calculate the mean and write out stuff
        cluster_sds = {}
        for cluster_index, cluster_center in enumerate(clustering.cluster_centers_):
            cluster_sds[cluster_index] = np.std(cluster_sequences[cluster_index])  # get the standard deviation
            csv_file.write('{}\t{}\t{}\t{}\t{}\n'.format(cluster_index, clustering.cluster_seq_counter[cluster_index],
                                                         clustering.cluster_sizes[cluster_index],
                                                         cluster_sds[cluster_index],
                                                         clustering.cluster_centers_[cluster_index].tolist()))
    # print plot
    # pca of the central points
    logger.info('Plotting PCA of clustering')
    pca = PCA(n_components=2, svd_solver='full').fit_transform(clustering.cluster_centers_)
    fig, ax = plt.subplots()
    x = [a[0] for a in pca]
    y = [a[1] for a in pca]
    max_point_size = 20
    scaling_factor = max_point_size/max(cluster_sds.values())
    area = [cluster_sds[key]*scaling_factor for key in sorted(cluster_sds.keys())]
    ax.scatter(x, y, s=area)
    for index in range(len(x)):  # add point labels
        ax.annotate(str(index).zfill(len(str(highest_cluster_index))), (x[index], y[index]))
    fig.savefig(os.path.join(cli_args.o, 'clustering_pca.png'), dpi=600)

if __name__ == '__main__':
    main()
