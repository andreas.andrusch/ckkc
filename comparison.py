import CKKC
import profile_fastas

import os

import numpy as np
from sklearn.metrics.pairwise import euclidean_distances
from sklearn.decomposition import PCA
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

infiles = ['vitor_clustering_stats.csv', 'andy_clustering_stats.csv']
outpath = '/home/andruscha/scratch_nobak/projects/ckkc_arc_bac/comparison'

clustering = profile_fastas.FakeClustering()

def read_in_csv(csv_path, prefix=0):
    clusters = []
    with open(csv_path) as infile:
        for index, line in enumerate(infile):
            if index == 0:
                continue
            line_split = line.rstrip().split('\t')
            clusters.append(convert_csv_line_split(line_split))  # cluster_number, number_of_sequences, number_of_bases, sd, center
            clusters[-1][0] += prefix
    return clusters

def convert_csv_line_split(line_split):
    res = []
    for index, item in enumerate(line_split):
        if index < 3: # 0-2
            res.append(int(item))
        elif index == 3:
            res.append(float(item))
        else:
            res.append(eval(item))  # oh yeah! eval in the house
    return res

# read in data
clusters = []
for index, infile in enumerate(infiles):
    clusters.extend(read_in_csv(os.path.join(outpath, infile), index*100))

cluster_sds = {}
for cluster in clusters:
    cluster_sds[cluster[0]] = cluster[3]

highest_cluster_index = clusters[-1][0]

# produce statistics
# print plot
# pca of the central points
pca = PCA(n_components=2, svd_solver='full').fit_transform([x[4] for x in clusters])
fig, ax = plt.subplots()
x = [a[0] for a in pca]
y = [a[1] for a in pca]
max_point_size = 50
scaling_factor = max_point_size / max([x[3] for x in clusters])
area = [cluster_sds[key] * scaling_factor for key in sorted(cluster_sds.keys())]
colors = ['b' if x[0] < 100 else 'r' for x in clusters]
ax.scatter(x, y, s=area, c=colors, alpha=0.5, edgecolors='none')
plt.xlabel('Principal Component 1')
plt.ylabel('Principal Component 2')
blue_patch = mpatches.Patch(color='b', alpha=0.5, label='Taxonomical clustering')
red_patch = mpatches.Patch(color='r', alpha=0.5, label='Sequence similarity clustering')
white_patch = mpatches.Patch(color='white', label='Spot size indicates standard deviation of cluster')
plt.legend(handles=[blue_patch, red_patch, white_patch])
#for index in range(len(x)):  # add point labels
#    ax.annotate(str(index).zfill(len(str(highest_cluster_index))), (x[index], y[index]))
fig.savefig(os.path.join(outpath, 'clustering_pca.png'), dpi=600)

# distance matrix
distances = euclidean_distances([x[4] for x in clusters])
with open(os.path.join(outpath, 'distance_matrix.csv'), 'w') as outfile:
    header = ['']
    header.extend([str(x[0]) for x in clusters])
    outfile.write('\t'.join(header))
    outfile.write('\n')
    for index, distance_line in enumerate(distances):
        left = [str(clusters[index][0])]
        left.extend([str(x) for x in distance_line.tolist()])
        outfile.write('\t'.join(left))
        outfile.write('\n')
