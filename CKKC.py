#!/usr/bin/env python3

# imports
# standard lib
import argparse
import functools
import gzip
import logging
import multiprocessing
import os
import pickle
import subprocess
import sys
import time

# third party
import numpy as np
from sklearn.cluster import KMeans
from sklearn.metrics.pairwise import euclidean_distances
from sklearn.decomposition import PCA
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

# constants
version = '1.0.0'  # program version
bases_to_ints = {'A': 0, 'C': 1, 'G': 2, 'T': 3}  # dict for hashing kmers
ints_to_bases = {0: 'A', 1: 'C', 2: 'G', 3: 'T'}  # dicts for reversing kmer hashes
complement_dict = {'-': '-', 'A': 'T', 'B': 'V', 'C': 'G',
                   'D': 'H', 'G': 'C', 'H': 'D', 'K': 'M',
                   'M': 'K', 'N': 'N', 'R': 'Y', 'S': 'S',
                   'T': 'A', 'V': 'B', 'W': 'W', 'Y': 'R'}
complement_dict_4_bases = {'A': 'T', 'C': 'G', 'G': 'C', 'T': 'A'}


def setup_parser():
    """
    This function creates the command line argument parser.
    
    :return: argparse.ArgumentParser object with all necessary arguments
    """

    parser = argparse.ArgumentParser(prog='CKKC', description='CKKC (canonical k-mer k-means clustering) clusters the sequences from a given multi-fasta file by applying a k-means clustering after characterising them through a length-normalized canonical k-mer count.')

    subparsers = parser.add_subparsers(help='choose whether to create a new clustering or update an existing one with new sequences', dest='subparser')

    parser_new = subparsers.add_parser('new', help='choose this subprogram to create a new clustering')

    parser_new.add_argument('-k', '--k-mer-length', default=9, type=int, help='set the length of the canonical k-mers used to profile the sequences (default: 9)')
    parser_new.add_argument('-u', '--upper-limit', default=20000, type=int, help='set the upper limit for the size of a single cluster in megabytes (default: 20000)')
    parser_new.add_argument('-l', '--lower-limit', default=300, type=int, help='set the lower limit for the size of a single cluster in megabytes (default: 300)')
    parser_new.add_argument('-n', '--cluster-number', default=50, type=int, help='set the number of clusters to generate during the first iteration (default: 50)')
    parser_new.add_argument('-f', '--force-cluster-number', default=False, action='store_true', help='set to not perform iterative cluster size refinement and just keep the first clustering (default: false)')

    parser_up = subparsers.add_parser('update', help='choose this subprogram to update an existing clustering')

    parser.add_argument('-V', '--version', action='version', version='{0} {1}'.format(parser.prog, version), help='output the version of the program')
    parser.add_argument('-o', '--output-folder', default=os.getcwd(), help='set the folder to which the results are written; if updating the cluster information is loaded from here (default: current working directory)')
    parser.add_argument('-t', '--temp-folder', default=os.path.join(os.getcwd(), 'temp'), help='set the folder to use for the temporary files created during execution (default: temp folder in the current working directory)')
    parser.add_argument('-j', '--jobs', default=multiprocessing.cpu_count(), type=int, help='set the number of subprocesses/threads to use for calculations (default: number of CPUs in the system)')
    parser.add_argument('-i', '--input', default='', required=True, help='path to the input multi-fasta file to cluster/add to clustering')
    parser.add_argument('-v', '--verbosity', choices=['debug', 'info', 'warning', 'error'], default='info', help='set the verbosity of the program output (default: info)')
    parser.add_argument('-z', '--gzip', default=False, action='store_true', help='set to enable gzipping of result fastas (default: false)')
    return parser


def setup_logger(level):
    """
    This function creates a logger and sets its initial settings.

    :param level: String determining the logging verbosity level
    :return: logging.Logger object
    """

    # create logger
    logger = logging.getLogger('CKKC')
    logger.setLevel(level.upper())

    # create console handler
    console_handler = logging.StreamHandler()
    console_handler.setLevel(logging.DEBUG)

    # create formatter to give output in the desired format
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    # add formatter to console_handler
    console_handler.setFormatter(formatter)

    # add console_handler to logger
    logger.addHandler(console_handler)

    return logger


def fasta_read_generator(file_handler):
    """
    This function returns a generator that yields name (without the leading >) and sequence tuples from a fasta file of
    which the handler is given.
    
    :param file_handler: file object wrapper for the input fasta file 
    :return: generator yielding name, sequence tuples 
    """

    seq = []
    name = ''

    for line in file_handler:
        if line[0] == '>':
            sequence = ''.join(seq)
            if name:  # only yield when we already have all data for the first sequence
                yield name, sequence
            name = line.rstrip()[1:]  # omitting the leading >
            seq = []
        else:
            seq += [line.rstrip()]

    sequence = ''.join(seq)
    yield name, sequence  # don't forget the last sequence


def kmer_to_hash(kmer, bases_to_ints):
    """
    This function calculates a hash for a given kmer using the bases_to_ints dictionary.
    
    :param kmer: String kmer 
    :param bases_to_ints: dict {String base: int base code} only for the 4 bases ACGT
    :return: int kmer hash 
    """

    k_hash = 0
    for char in kmer.upper():
        k_hash = k_hash << 2
        k_hash = k_hash | bases_to_ints[char]

    return k_hash


def hash_to_kmer(k_hash, k_len, ints_to_bases):
    """
    This function calculates the corresponding kmer for the given kmer hash and the kmer length using a given
    ints_to_bases dictionary that contains mappings for ACGT.
    
    :param k_hash: int kmer hash 
    :param k_len: int original kmer length
    :param ints_to_bases: dict {int base code: String base}
    :return: String original kmer 
    """

    res = []
    last_2_bits_mask = 3
    for i in range(k_len):
        last_2_bits = k_hash & last_2_bits_mask
        res.append(ints_to_bases[last_2_bits])
        k_hash = k_hash >> 2

    return ''.join(res)[::-1]  # since we reconstruct from the back


def canonize_kmer(kmer, complement_dict):
    """
    This function canonizes (or normalizes) a kmer by return the lexicographically smaller choice from itself and its
    reverse complement.
    
    :param kmer: String kmer 
    :param complement_dict: dict {String base: String complement base}
    :return: String canonical version of input kmer
    """

    return min(kmer, reverse_complement_sequence(kmer, complement_dict))


def reverse_sequence(seq):
    """
    This function reverses a given DNA sequence.
    
    :param seq: String DNA sequence
    :return: String reversed DNA sequence
    """

    return seq[::-1]


def complement_sequence(seq, complement_dict):
    """
    This function calculates the complement of a given DNA sequence using the given complement_dict for the lookup.
    
    :param seq: String DNA sequence 
    :param complement_dict: dict {String base: String complement base}
    :return: String complement DNA sequence
    """

    return ''.join([complement_dict[char] for char in seq])


def reverse_complement_sequence(seq, complement_dict):
    """
    This function calculates the reverse complement of a given DNA sequence using the given complement_dict.
    
    :param seq: String DNA sequence
    :param complement_dict: dict {String base: String complement base}
    :return: String reverse complement DNA sequence
    """

    return reverse_sequence(complement_sequence(seq, complement_dict))


def calculate_possible_kmers(k, bases_to_int_dict):
    """
    This function calculates the number of possible kmers of size k using all keys (bases) in the given complement_dict.
    
    :param k: int kmer length
    :param bases_to_int_dict: dict {str base: int base code} where the keys are used for the calculation
    :return: int number of possible kmers
    """

    return len(bases_to_int_dict) ** k


def read_fasta(infile_handler, queue, ordered_seqs, length_dict, logger, known=None):
    """
    This function reads the entries from a (multi)fasta file given as a handler and updates a given queue for threaded
    or multiprocessed kmer counting as well as a list of tuples collecting general information of the sequences.
    
    :param infile_handler: file object wrapping the input fasta file
    :param queue: multiprocessing.Queue to allow thread-safe downstream operations
    :param ordered_seqs: List of String sequence names to recreate the sequence read order
    :param length_dict: dict {String sequence name: int sequence length} for normalizing
    :param logger: logging.logger object to which to write logging output
    :param known: set of sequence names of already profiled sequences - don't get put into the queue for profiling
    :return: None
    """

    seq_gen = fasta_read_generator(infile_handler)
    counter = 0
    for name, seq in seq_gen:
        ordered_seqs.append(name)  # this collects input order info
        # todo handle non-unique sequence names? warn at least ... or put in argparse description of program
        if known and name in known:  # already known sequence, presumably from an earlier clustering
            length_dict[name] = len(seq), None
            logger.debug('Skipping profiling for {}'.format(name))
        else:  # every other and therefore new sequence
            length_dict[name] = len(seq), None  # this collects sequence lengths for normalizing
            queue.put((counter, name, seq))  # this is used as a means of communication with the other threads
        counter += 1
        if not counter % 100:
            logger.info('Read {} sequences'.format(counter))
    logger.info('Read {} sequences'.format(counter))
    queue.put(None)  # Sentinel indicating end of input for readers
    queue.close()


def calculate_normalized_canonical_kmer_count_vector(seq, k, complement_dict, bases_to_ints):
    """
    This function calculates a length normalized list of canonical kmer counts.

    :param seq: String DNA sequence to produce the output for
    :param k: int kmer length
    :param complement_dict: dict {str base: str complement base}
    :param bases_to_ints: dict {str base: int base code}
    :return: list of length normalized canonical kmer counts
    """

    max_kmers = calculate_possible_kmers(k, bases_to_ints)
    kmers_in_seq = len(seq) - k + 1
    kmer_vec = [0] * max_kmers

    # find and count all kmers (consisting only of ACGT, ignore the rest)
    for i in range(kmers_in_seq):
        try:
            kmer = seq[i:i + k]
            c_kmer = canonize_kmer(kmer, complement_dict)
            ck_hash = kmer_to_hash(c_kmer, bases_to_ints)
            kmer_vec[ck_hash] += 1
        except KeyError:  # ignore kmers consisting of other letters
            pass

    normalized_kmer_vec = normalize_kmer_vector(kmer_vec)

    return normalized_kmer_vec
    # return csr_matrix(normalized_kmer_vec)


def thread_wrapper_for_kmer_counting(input_queue, output_queue, k, complement_dict, bases_to_ints, logger):
    while True:
        next_item = input_queue.get()
        if not next_item:  # Sentinel encountered - we quit
            input_queue.put(None)  # put it back for the other threads to see
            output_queue.close()
            output_queue.join()
            logger.debug('Returning from kmer profiling subprocess')
            return
        counter, name, sequence = next_item
        logger.debug('Fetched sequence {} for kmer profiling'.format(name))
        normkvec = calculate_normalized_canonical_kmer_count_vector(sequence, k, complement_dict, bases_to_ints)
        output_queue.put((counter, name, normkvec))
        logger.debug('Done profiling {}'.format(name))


def calculate_cluster_sizes(clustering, names, seq_len_kmer_vec_dict):
    """
    This function calculates the cluster sizes in bytes/sequence characters resulting from a k-means clustering.
    
    :param clustering: KMeans clustering object with the clusters of which the sizes shall be determined 
    :param names: list of names in the same order as data input into the clustering (e.g. clustering.labels_)
    :param seq_len_kmer_vec_dict: dict {str sequence_name: (int sequence_length, seq_norm_k_vector)} as out_dict
    :return: tuple (list [int cluster_seq_count], list [cluster_size])
    """

    cluster_seq_counter = [0] * clustering.n_clusters
    cluster_sizes = [0] * clustering.n_clusters
    for name_index, name in enumerate(names):
        cluster_index = clustering.labels_[name_index]
        cluster_seq_counter[cluster_index] += 1
        cluster_sizes[cluster_index] += seq_len_kmer_vec_dict[name][0]

    return cluster_seq_counter, cluster_sizes


def check_cluster_sizes(cluster_sizes, cluster_sequence_count, lower_limit, upper_limit, logger):
    """
    This function checks which clusters are too big or too small by the given limits and returns a tuple of sets with
    indices marking the clusters failing the size limits.
    
    :param cluster_sizes: list of cluster size integers in bytes/sequence characters
    :param cluster_sequence_count: list of counts of sequences in the clusters, same order as cluster_sizes
    :param lower_limit: integer lower limit in bytes/characters for a cluster
    :param upper_limit: integer upper limit in bytes/characters for a cluster
    :param logger: logging.logger object to which to write logging output
    :return: tuple (set too_small, set too_big) - sets contain indices of the clusters corresponding to the input list 
    """

    too_small = set()
    too_big = set()
    for cluster_index, cluster_size in enumerate(cluster_sizes):
        if cluster_size < lower_limit:
            if cluster_size == 0:
                logger.info('Cluster {} is empty, ignoring'.format(cluster_index))
            else:
                too_small.add(cluster_index)
        elif cluster_size > upper_limit:
            if cluster_sequence_count[cluster_index] == 1:
                logger.warning('CLUSTER {} TOO BIG, BUT CONTAINS ONLY ONE SEQUENCE, NOT SPLITTING'.
                               format(cluster_index))
            else:
                too_big.add(cluster_index)

    return too_small, too_big


def calculate_cluster_centers(n_clusters, labels, norm_k_vectors):
    """
    
    :param n_clusters: int number of clusters
    :param labels: list of labels as returned by a clustering
    :param norm_k_vectors: list of normalized kmer vectors in the same order as labels
    :return: np array of cluster center norm k vectors in sorted by cluster id order [center_0, center_1, ...]
    """

    centers = []
    empty_k_vec = [0.] * len(norm_k_vectors[0])  # take first norm_k_vector and produce an empty vec of same len
    for i in range(n_clusters):
            centers.append(empty_k_vec[:])  # copy not same instance

    for index, name in enumerate(labels):
        label = labels[index]
        norm_k_vec = norm_k_vectors[index]
        centers[label] = list(np.add(centers[label], norm_k_vec))  # element wise addition of every norm k vec

    return np.array([normalize_kmer_vector(x) for x in centers], dtype=np.float64)


def normalize_kmer_vector(k_vec):
    """
    This function normalizes a given k-mer vector.
    
    :param k_vec: k-mer vector, i.e. list of k-mer frequencies or counts 
    :return: normalized k-mer vector, i.e. list of k-mer frequencies, with sum = 1  
    """

    kmer_sum = sum(k_vec)
    if kmer_sum == 0:
        return [float(x) for x in k_vec]
    else:
        return [float(x)/kmer_sum for x in k_vec]


def calculate_normalized_canonical_kmer_count_vector_wrapper(name_seq_tuple, k, complement_dict, bases_to_ints):
    return name_seq_tuple[0], len(name_seq_tuple[1]), \
           calculate_normalized_canonical_kmer_count_vector(name_seq_tuple[1], k, complement_dict, bases_to_ints)


def get_chunk_from_fasta_iterator(iterator, seqbytes=10000000000):
    broken = False
    while True:
        out = []
        bytes = 0
        while bytes < seqbytes:
            try:
                item = next(iterator)
                bytes += len(item[1])
                out.append(item)
            except StopIteration:
                yield out
                broken = True
                break
        if broken:
            break
        yield out

def read_fasta_input_to_norm_k_vectors_threaded(fasta_handler, kmer_length, jobs, logger, complement_dict,
                                                bases_to_ints, known=None):
    # todo use known
    # todo more verbosity
    # todo get rid of old unreferenced code

    # setup
    out_dict = {}
    if not known:
      known = {}
    fasta_gen = fasta_read_generator(fasta_handler)
    mapping_func = functools.partial(calculate_normalized_canonical_kmer_count_vector_wrapper, k=kmer_length,
                                     complement_dict=complement_dict, bases_to_ints=bases_to_ints)

    # get chunks of sequences to process
    # todo expose chunk size as cli parameter?
    chunk_iter = get_chunk_from_fasta_iterator(fasta_gen, jobs*500000000)  # memory scales with number of jobs
    logger.info('Start reading in sequences')
    for chunk in chunk_iter:
        logger.info('Processing next chunk of sequences')
        with multiprocessing.Pool(processes=jobs, maxtasksperchild=1000) as pool:  # maxtasks to free up memory
            result = pool.map(mapping_func, chunk)

        for tuple in result:
            out_dict[tuple[0]] = tuple[1:]
    logger.info('Finished reading in and profiling sequences')

    # return as dictionary
    return out_dict


def kanalyze_output_to_vector(kanalyze_output, kmer_length):
    kmer_vec = [0]*4**kmer_length
    kanalyze_output_split = kanalyze_output.split('\n')
    for line in kanalyze_output_split:
        if line == '':
            continue
        line_split = line.rstrip().split('\t')
        kmer = line_split[0]
        count = int(line_split[1])
        hash = kmer_to_hash(kmer, bases_to_ints)
        kmer_vec[hash] = count
    return kmer_vec


def read_fasta_input_to_norm_k_vectors_external(fasta_handler, kmer_length, jobs, logger, path_to_kanalyze):
    out_dict = {}
    fasta_gen = fasta_read_generator(fasta_handler)

    # pass sequence to kanalyze and collect kmer profile
    counter = 0
    start = time.time()
    logger.info('Start reading sequences from file and passing them to KAnalyze')
    for name, seq in fasta_gen:
        counter += 1
        now = time.time()
        if now-start > 3:
            start = now
            logger.info('Read {} sequences'.format(counter))
        seq_len = len(seq)
        kanalyze_args = (path_to_kanalyze, '-k', str(kmer_length), '-f', 'fasta', '-rcanonical', '--stdin', '--stdout',
                         '-t', str(jobs))
        kanalyze_proc = subprocess.Popen(kanalyze_args, stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                                         universal_newlines=True)
        stdout, _ = kanalyze_proc.communicate('>{}\n{}\n'.format(name, seq))
        kanalyze_proc.wait()
        kmer_vec = kanalyze_output_to_vector(stdout, kmer_length)
        out_dict[name] = seq_len, kmer_vec

    return out_dict


def main():
    # setup argument parser and parse command line arguments
    parser = setup_parser()
    cli_args = parser.parse_args(sys.argv[1:])

    # setup logging facility
    logger = setup_logger(cli_args.verbosity)

    # log args summary
    logger.info('Started CKKC')
    logger.debug('Created Logger')
    logger.debug('Parsed CLI Arguments: {}'.format(cli_args))

    # create output directory if necessary
    try:
        os.makedirs(cli_args.output_folder)
        logger.debug('Created output folder')
    except OSError:
        logger.debug('Output folder already exists')

    # open input source
    if not cli_args.input.endswith('.gz'):
        fasta_handler = open(cli_args.input)
    else:
        fasta_handler = gzip.open(cli_args.input, 'rt')
    logger.debug('Opened {} as input'.format(cli_args.input))

    # determine in which mode to operate
    clustering = None
    if cli_args.subparser == 'new':
        logger.info('Selected new mode')

        # set limits to workable numbers instead of easier human readability
        cli_args.upper_limit *= 1000000  # convert mb to bytes which we can compare against seq lengths
        cli_args.lower_limit *= 1000000

        # read in data with several jobs
        kanalyze_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'kanalyze-2.0.0', 'count')
        out_dict = read_fasta_input_to_norm_k_vectors_external(fasta_handler, cli_args.k_mer_length, cli_args.jobs,
                                                               logger, kanalyze_path)

        # cluster
        logger.info('Starting clustering')
        names_n_norms = ((name, norm_k_vector) for name, (seq_len, norm_k_vector) in out_dict.items())
        names, norm_k_vectors = zip(*names_n_norms)
        clustering = KMeans(cli_args.cluster_number, n_jobs=cli_args.jobs, copy_x=False)  # save memory by not copying?
        clustering.fit(norm_k_vectors)
        # add other infos to cluster
        clustering.sequence_names = list(names)  # have to be lists instead of tuples so we can delete during updates
        clustering.norm_k_vectors = list(norm_k_vectors)
        clustering.sequence_lengths = [out_dict[name][0] for name in clustering.sequence_names]
        logger.info('Finished clustering')

        # iterative refinement if desired
        # todo document refinement approach in the argparse program description
        if not cli_args.force_cluster_number:
            logger.info('Refining clustering')
            # refine until good
            counter = 0
            last_too_small_too_big = (-1, -1)
            while True:
                counter += 1
                logger.info('Begin refinement round {}'.format(counter))

                # calculate cluster sizes
                logger.info('Calculating cluster sizes')
                clustering.cluster_seq_counter, clustering.cluster_sizes =\
                    calculate_cluster_sizes(clustering, clustering.sequence_names, out_dict)
                logger.debug('Cluster sequence counter: {}'.format(clustering.cluster_seq_counter))
                logger.debug('Cluster sizes: {}'.format(clustering.cluster_sizes))
                logger.info('Done calculating cluster sizes')

                # check if any cluster is too big or small
                logger.info('Checking for cluster sizes outside of defined parameters')
                too_small, too_big = check_cluster_sizes(clustering.cluster_sizes, clustering.cluster_seq_counter,
                                                         cli_args.lower_limit, cli_args.upper_limit, logger)
                logger.debug('Too small: {}'.format(sorted(list(too_small))))
                logger.debug('Too big: {}'.format(sorted(list(too_big))))
                logger.info('Done checking clusters sizes: Found {} too small and {} too big clusters'
                            .format(len(too_small), len(too_big)))

                # break if "converged"
                if (too_small, too_big) == last_too_small_too_big:
                    logger.info('Seemingly reached convergence ... breaking from refinements')
                    break
                else:
                    last_too_small_too_big = (too_small, too_big)

                # if all clusters are alright, exit this loop
                if not too_small.union(too_big):
                    logger.info('Cluster refinement done')
                    break

                # join clusters that are too small with their next cluster
                if too_small:
                    logger.info('Processing clusters that are too small')

                    if len(too_small) == clustering.n_clusters:
                        logger.warning('ALL CLUSTERS TOO SMALL, JOINING ALL CLUSTERS (CHECK YOUR PARAMETER SETTINGS)')
                        # substitute labels with same length list of zeroes
                        clustering.labels_ = np.asarray([0] * clustering.labels_.shape[0], dtype=np.int32)
                        clustering.n_clusters = 1

                    else:  # only some too small
                        # calculate distances between cluster centers
                        logger.info('Calculate distances between cluster centers')
                        distances_between_centers = euclidean_distances(clustering.cluster_centers_)
                        logger.debug('Distance matrix:\n{}'.format(distances_between_centers))
                        logger.info('Done calculating distances between cluster centers')

                        # join clusters / assign sequences from unwanted clusters to nearest clusters
                        for cluster_index in too_small:
                            # find nearest neighbor (except self and other too smalls)
                            distance_index = cluster_index
                            sorted_distances = sorted(distances_between_centers[cluster_index])
                            for distance in sorted_distances[1:]:
                                distance_index = np.nonzero(distances_between_centers[cluster_index] == distance)[0][0]
                                # we join to nearest cluster that itself is not too small
                                # todo join to nearest and then see if it's still too small
                                if distance_index not in too_small:
                                    break

                            # substitute all labels
                            clustering.labels_ = np.array([x if x != cluster_index else distance_index for x in
                                                  clustering.labels_], dtype=np.int32)

                            # reduce cluster number
                            #clustering.n_clusters -= 1
                            # we're not doing this right now because of arising downstream problems, therefore
                            # todo cleanly remove empty clusters + update all other indices

                            # recalculate cluster centers
                            # np structure for center re-calculations
                            logger.info('Recalculating cluster centers')
                            clustering.cluster_centers_ = calculate_cluster_centers(clustering.n_clusters,
                                                                                    clustering.labels_,
                                                                                    clustering.norm_k_vectors)

                # split every cluster that is too big in two by reclustering
                if too_big:
                    logger.info('Processing clusters that are too big')

                    # for each cluster that is too big
                    for cluster_index in too_big:
                        next_cluster_index = max(clustering.labels_) + 1

                        # extract data points / mapping to old indices used to re-label the original clustering
                        new_nkvs = []  # new norm k vectors
                        mapping_to_old_indices = []
                        for index, (nkv, label) in enumerate(zip(clustering.norm_k_vectors, clustering.labels_)):
                            if label == cluster_index:
                                new_nkvs.append(nkv)
                                mapping_to_old_indices.append(index)

                        # re-cluster
                        new_clustering = KMeans(n_clusters=2, n_jobs=cli_args.jobs, copy_x=True)  # just split in 2
                        new_clustering.fit(new_nkvs)

                        # from the reclustering update all labels of original clustering
                        for index, label in enumerate(new_clustering.labels_):
                            if label == 1:  # we change the labels of all sequences in the second new cluster
                                clustering.labels_[mapping_to_old_indices[index]] = next_cluster_index

                        # update cluster number
                        clustering.n_clusters += 1

                    # recalculate cluster centers after finishing the too big clusters
                    # np structure for center re-calculations
                    logger.info('Recalculating cluster centers')
                    clustering.cluster_centers_ = calculate_cluster_centers(clustering.n_clusters,
                                                                            clustering.labels_,
                                                                            clustering.norm_k_vectors)

        # save settings for reuse on update
        with open(os.path.join(cli_args.output_folder, 'arguments.pickle'), 'wb') as outfile:
            pickle.dump(cli_args, outfile, pickle.HIGHEST_PROTOCOL)
            pickle.dump(version, outfile, pickle.HIGHEST_PROTOCOL)

    elif cli_args.subparser == 'update':
        logger.info('Selected update mode')
        # load information/results from output folder
        clustering = pickle.load(open(os.path.join(cli_args.output_folder, 'clustering.pickle'), 'rb'))
        old_args = pickle.load(open(os.path.join(cli_args.output_folder, 'arguments.pickle'), 'rb'))
        # profile new sequences
        known_sequences = set(clustering.sequence_names)
        out_dict = read_fasta_input_to_norm_k_vectors_threaded(fasta_handler, old_args.k_mer_length, cli_args.jobs,
                                                               logger, known=known_sequences)

        # make sets out of the old and new sequence names to be able to compare differences
        new_sequence_names = {name for name in out_dict}
        old_sequence_names = set(clustering.sequence_names)

        # find sequences newly added as well as removed sequences
        added = new_sequence_names.difference(old_sequence_names)
        removed = old_sequence_names.difference(new_sequence_names)
        logger.info('Found {} new sequences to add'.format(len(added)))
        logger.info('Found {} old sequences which will be removed'.format(len(removed)))

        # process differences
        # removed sequences first
        logger.info('Removing sequences')

        # get indices to remove
        indices_to_remove = []
        for index, sequence_name in enumerate(clustering.sequence_names):
            if sequence_name in removed:
                indices_to_remove.append(index)

        for index_to_remove in sorted(indices_to_remove, reverse=True):  # going backwards to prevent index fuckups
            # adjust cluster sizes and sequence counter
            cluster_index = clustering.labels_[index_to_remove]
            clustering.cluster_seq_counter[cluster_index] -= 1
            clustering.cluster_sizes[cluster_index] -= clustering.sequence_lengths[index_to_remove]

            # del sequence data
            clustering.labels_ = np.delete(clustering.labels_, index_to_remove)
            sequence_name = clustering.sequence_names[index_to_remove]
            del clustering.sequence_names[index_to_remove]
            del clustering.sequence_lengths[index_to_remove]
            del clustering.norm_k_vectors[index_to_remove]

            logger.debug('Removed {}'.format(sequence_name))

        logger.info('Done removing sequences')

        # add new sequences
        logger.info('Adding sequences')
        for sequence_name in added:
            # get length and norm k vec
            sequence_length, norm_k_vector = out_dict[sequence_name]

            # calculate distances to clusters and determine nearest cluster
            nkv = np.array(norm_k_vector, dtype=np.float64, copy=True)
            nkv = nkv.reshape(1, nkv.shape[0])
            distances_to_centers = euclidean_distances(nkv, clustering.cluster_centers_)
            min_dist, cluster_index = min((dist, index) for index, dist in enumerate(distances_to_centers[0]))

            # add to nearest cluster, updating cluster seq counter and size
            clustering.sequence_names.append(sequence_name)
            clustering.sequence_lengths.append(sequence_length)
            clustering.norm_k_vectors.append(norm_k_vector)
            np.append(clustering.labels_, cluster_index)
            clustering.cluster_seq_counter[cluster_index] += 1
            clustering.cluster_sizes[cluster_index] += sequence_length

            logger.debug('Added {}'.format(sequence_name))

        logger.info('Done adding sequences')

        # recalculate centers
        logger.info('Recalculating cluster centers')
        clustering.cluster_centers_ = calculate_cluster_centers(clustering.n_clusters, clustering.labels_,
                                                                clustering.norm_k_vectors)

    else:  # no mode chosen
        logger.warning('Please choose a mode of operation!')
        exit(1)

    # after creating or updating a clustering
    # save results (clusters, centers, labels, count, size as well as sequence names, lenghts, and vecs) in pickles
    logger.info('Saving clustering')
    with open(os.path.join(cli_args.output_folder, 'clustering.pickle'), 'wb') as outfile:
        pickle.dump(clustering, outfile, pickle.HIGHEST_PROTOCOL)

    # save clusterings as csv
    logger.info('Generating sequence name to cluster table')
    with open(os.path.join(cli_args.output_folder, 'sequence_cluster_mapping.csv'), 'w') as outfile:
        for name, cluster in zip(clustering.sequence_names, clustering.labels_):
            outfile.write('{}\t{}\n'.format(name, cluster))

    # split into fasta files
    logger.info('Splitting sequences into fasta files corresponding to generated clusters')
    # open all possible output files
    open_files = []
    highest_cluster_index = max(clustering.labels_)
    for i in range(highest_cluster_index+1):
        basename = str(i).zfill(len(str(highest_cluster_index)))
        if cli_args.gzip:
            open_files.append(gzip.open(os.path.join(cli_args.output_folder, basename+'.fa.gz'), 'wt', 6))
        else:
            open_files.append(open(os.path.join(cli_args.output_folder, basename+'.fa'), 'w'))

    # create lookup dictionary sequence name -> cluster index
    logger.info('Creating lookup dictionary for sequence name to cluster index assignment')
    index_lookup_dict = {}
    for seq_name, cluster_index in zip(clustering.sequence_names, clustering.labels_):
        index_lookup_dict[seq_name] = cluster_index

    # go through input fasta and write out cluster fastas
    logger.info('Writing fastas')
    fasta_handler.seek(0)  # reset fasta file to the beginning, so we can read again ...
    seq_gen = fasta_read_generator(fasta_handler)
    for name, seq in seq_gen:
        cluster_index = index_lookup_dict[name]
        open_files[cluster_index].write('>{}\n{}\n'.format(name, seq))
    for open_file in open_files:
        open_file.close()

    # produce statistics
    # write out cluster num, seq num, base num, standard deviation, (weighted sd), central point
    logger.info('Generating statistics for clustering')
    with open(os.path.join(cli_args.output_folder, 'clustering_stats.csv'), 'w') as csv_file:
        csv_file.write('Cluster\tNumber of sequences\tNumber of bases\tStandard deviation\tCentral point\n')
        # get all sequences of every cluster into one list to calculate sd
        cluster_sequences = {}
        used_labels = set()
        for sequence_index, cluster_label in enumerate(clustering.labels_):
            used_labels.add(cluster_label)
            try:
                cluster_sequences[cluster_label].append(clustering.norm_k_vectors[sequence_index])
            except KeyError:
                cluster_sequences[cluster_label] = [clustering.norm_k_vectors[sequence_index]]
        # calculate the mean and write out stuff
        cluster_sds = {}
        for cluster_index in sorted(used_labels):
            cluster_sds[cluster_index] = np.std(cluster_sequences[cluster_index])  # get the standard deviation
            csv_file.write('{}\t{}\t{}\t{}\t{}\n'.format(cluster_index, clustering.cluster_seq_counter[cluster_index],
                                                         clustering.cluster_sizes[cluster_index],
                                                         cluster_sds[cluster_index],
                                                         clustering.cluster_centers_[cluster_index].tolist()))

    # print plot
    # pca of the central points
    logger.info('Plotting PCA of clustering')
    pca = PCA(n_components=2, svd_solver='full').fit_transform(clustering.cluster_centers_)
    fig, ax = plt.subplots()
    x = [a[0] for a in pca]
    y = [a[1] for a in pca]
    max_point_size = 20
    scaling_factor = max_point_size/max(cluster_sds.values())
    area = [cluster_sds[key]*scaling_factor for key in sorted(cluster_sds.keys())]
    ax.scatter(x, y, s=area)
    for index in range(len(x)):  # add point labels
        ax.annotate(str(index).zfill(len(str(highest_cluster_index))), (x[index], y[index]))
    fig.savefig(os.path.join(cli_args.output_folder, 'clustering_pca.png'), dpi=600)

    # close and finish
    logger.info('Done!')

    return clustering


if __name__ == '__main__':
    clustering = main()
